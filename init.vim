let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

call plug#begin()
" The default plugin directory will be as follows:
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'

" Make sure you use single quotes

" a universal set of defaults that (hopefully) everyone can agree on
Plug 'junegunn/vim-plug'

" Plug 'rafi/awesome-vim-colorschemes'
" Material Design Theme
Plug 'NLKNguyen/papercolor-theme'

" Filebrowser lf
Plug 'ptzz/lf.vim'
Plug 'voldikss/vim-floaterm'

" fzf is a general-purpose command-line fuzzy finder.
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" Tab/Space trough projects
Plug 'editorconfig/editorconfig-vim'

" Neovim plugin that allows you to seamlessly install LSP servers locally
Plug 'neovim/nvim-lspconfig'
Plug 'williamboman/nvim-lsp-installer'

" collection of language packs
Plug 'sheerun/vim-polyglot'

" Autocompletion
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'saadparwaiz1/cmp_luasnip'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-nvim-lua'

" Auto close parentheses and repeat by dot dot dot
Plug 'cohama/lexima.vim'

"  Snippets
Plug 'L3MON4D3/LuaSnip'
Plug 'rafamadriz/friendly-snippets'

" plugin is to bundle all the boilerplate code necessary to get nvim-cmp (a popular completion engine) and the native LSP client to work together nicely
Plug 'VonHeikemen/lsp-zero.nvim'

" Tabbar
Plug 'kyazdani42/nvim-web-devicons'
Plug 'romgrk/barbar.nvim'

" Initialize plugin system
call plug#end()

" init LSP
lua <<EOF
local lsp = require('lsp-zero')

lsp.preset('recommended')
lsp.setup()
EOF

set mouse+=a

set background=dark
colorscheme PaperColor

:set relativenumber
